# gojek_workspace

Using IDE (Preferably PyCharm) :

    1. go to gojek_workspace/api_automation/tests
    2. right click on compare_test.py and Run 'pytest in compare_test...'

Using Command Line:
    1. Create a virtual environment under this project (python3.7 -m venv venv)
    2. Now activate the venv (source venv/bin/activate)
    3. Go to tests module and run the command (pytest compare_test.py)

Note : If you want to run 1000 requests, just go to gojek_workspace/api_automation/tests/compare_test.py and
       change the index of file path to 1 (ex. first_file_path[1]) line no (11 and 12)


