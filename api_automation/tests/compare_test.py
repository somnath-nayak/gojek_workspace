import pytest
from api_automation.helper.common_helper import *
from api_automation.constants.api_constants import *


class TestCompareApiResponse:

    @pytest.mark.description("Comparing two API responses")
    def test_compare_response(self):

        first_file_lines = CommonHelper.get_requests_from_file(first_file_path[0])
        second_file_lines = CommonHelper.get_requests_from_file(second_file_path[0])

        comparable_lines = min(len(first_file_lines), len(second_file_lines))

        for i in range(comparable_lines):
            try:
                first_resp = CommonHelper.api_call(first_file_lines[i])
                second_resp = CommonHelper.api_call(second_file_lines[i])

                if first_resp.status_code == second_resp.status_code == 200:
                    first_json = first_resp.json()
                    second_json = second_resp.json()

                    result = CommonHelper.compare_json(first_json, second_json)

                    if result:
                        print('{0} equals {1}'.format(first_file_lines[i], second_file_lines[i]))
                    else:
                        print('{0} not equals {1}'.format(first_file_lines[i], second_file_lines[i]))

            except Exception as e:
                print(str(e))
