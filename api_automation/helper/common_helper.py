import requests


class CommonHelper:

    @staticmethod
    def get_requests_from_file(file_path):
        file = open(file_path, 'r')
        lines = file.readlines()
        return lines

    @staticmethod
    def compare_json(first_resp, second_resp):
        result = sorted(first_resp.items()) == sorted(second_resp.items())
        return result

    @staticmethod
    def api_call(url):
        response = requests.get(url)
        return response
